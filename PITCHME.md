# Erste GitPitch Slides für Matlab

---

## Bilder ausprobieren

![IMAGE](assets/img/MagicFormulaLeister.PNG)

---?color=blue
@title[Customize Slide Layout]

@snap[north span-100]
## Code ausprobieren
@snapend



```matlab
% Erste Möglichkeit: Den Plot gleich als Variable speichern.
f = scatter(Data(:,1), Data(:,2));

%%
f.Marker = '*';
f.MarkerEdgeColor = 'g';

% Achsen verändern:
ax = gca;
ax.XLabel.String = 'Schlupf [%]';
grid on;
```

---?color=linear-gradient(90deg, #5384AD 65%, white 35%)
@title[Add A Little Imagination]

@snap[north-west h3-white]
### Und hier noch ein paar Präsentationspunkte...
@snapend

@snap[west span-65]
@ul[list-spaced-bullets text-white text-09]
- Keine Vermittlung von Grundlagen in Matlab
- *Bei Bedarf:* [Matlab Academy @fa[external-link]](https://matlabacademy.mathworks.com/) 
- **Wichtig:** Matlab Dokumentation
@ulend
@snapend

@snap[east span-35]
@img[shadow](assets/img/conference.png)
@snapend


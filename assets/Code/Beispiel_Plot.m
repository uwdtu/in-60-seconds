clc; close all; clearvars;

homedir = pwd;
cd('C:\Users\Frank\Desktop\UNI\8. Semester\HIWI\Lehrveranstaltung\Matlab Aufgaben\Aufgabe 1');
load('CurveToFit.mat');
cd(homedir);

% Erste Möglichkeit: Den Plot gleich als Variable speichern.
f = scatter(Data(:,1), Data(:,2));

%%
f.Marker = '*';
f.MarkerEdgeColor = 'g';


% Achsen verändern:
ax = gca;
ax.XLabel.String = 'Schlupf [%]';
grid on;










